<?php

namespace App\Command\WebMoneyCommand;

use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class PursePassportWebMoneyCommand extends PassportWebMoneyCommand
{
    private const ARGUMENT_PURSE = 'purse';

    /**
     * @throws InvalidArgumentException
     */
    protected function configure(): void
    {
        $this->setName('wm:info:purse');
        $this->setDescription('Displays information about WebMoney user by purse number');
        $this->addArgument(self::ARGUMENT_PURSE, InputArgument::REQUIRED, 'Purse number');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @throws InvalidArgumentException
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $output->write(
            $this->serialize(
                $this->webMoneyPassport->getInfoByPurse(
                    $input->getArgument(self::ARGUMENT_PURSE)
                )
            )
        );
    }
}
