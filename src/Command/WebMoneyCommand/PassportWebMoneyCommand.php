<?php

namespace App\Command\WebMoneyCommand;

use App\Contract\WebMoneyPassportInterface;
use JMS\Serializer\Serializer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\LogicException;

abstract class PassportWebMoneyCommand extends Command
{
    /**
     * @var WebMoneyPassportInterface
     */
    protected $webMoneyPassport;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @param WebMoneyPassportInterface $webMoneyPassport
     * @param Serializer                $serializer
     *
     * @throws LogicException
     */
    public function __construct(WebMoneyPassportInterface $webMoneyPassport, Serializer $serializer)
    {
        parent::__construct();

        $this->webMoneyPassport = $webMoneyPassport;
        $this->serializer = $serializer;
    }

    /**
     * @param mixed  $data
     * @param string $format
     *
     * @return string
     */
    protected function serialize($data, string $format = 'yml'): string {
        return $this->serializer->serialize($data, $format);
    }
}
