<?php

namespace App\Command\WebMoneyCommand;

use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class WmidPassportWebMoneyCommand extends PassportWebMoneyCommand
{
    private const ARGUMENT_WMID = 'wmid';

    /**
     * @throws InvalidArgumentException
     */
    protected function configure(): void
    {
        $this->setName('wm:info:wmid');
        $this->setDescription('Displays information about WebMoney user by WMID');
        $this->addArgument(self::ARGUMENT_WMID, InputArgument::REQUIRED, 'WMID');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @throws InvalidArgumentException
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $output->write(
            $this->serialize(
                $this->webMoneyPassport->getInfoByWmid(
                    $input->getArgument(self::ARGUMENT_WMID)
                )
            )
        );
    }
}
