<?php

namespace App\DTO;

class Certificate
{
    /**
     * Внутренний id пользователя (не WMID)
     *
     * @var int
     */
    private $id;

    /**
     * Тип аттестата
     *
     * 100 - аттестат псевдонима
     * 110 - формальный аттестат
     * 120 - начальный аттестат
     * 130 - персональный аттестат
     * 135 - аттестат продавца
     * 136 - аттестат capitaller, учрежденный группой физических лиц
     * 137 - аттестат capitaller, учрежденный юридическим лицом
     * 138 - аттестат расчетного автомата
     * 140 - аттестат разработчика
     * 150 - аттестат регистратора
     * 170 - аттестат гаранта
     * 190 - аттестат сервиса wmt
     * 200 - аттестат сервиса wmt
     * 300 - аттестат оператора
     *
     * @var int
     */
    private $type;

    /**
     * Регистратор
     *
     * @var Registrant
     */
    private $registrant;

    /**
     * Признак того, что владелец данного WMID имеет право на выдачу аттестатов
     *
     * @var bool
     */
    private $rightToIssue;

    /**
     * Признак того, что владелец данного WMID занимается выдачей аттестактов
     *
     * @var bool
     */
    private $admRightToIssue;

    /**
     * Признак отказа в обслуживании
     *
     * @var bool
     */
    private $recalled;

    /**
     * Дата и время (московское) выдачи аттестата в формате гггг-мм-ддТчч:мм:сс.ммм
     *
     * @var string
     */
    private $dateIssuance;

    /**
     * Хрен знает что это такое в апи называется "response/certinfo/attestat/row/@datediff" (это xpath)
     *
     * @var int
     */
    private $dateDiff;

    /**
     * Статус прохождения вторичной проверки
     *
     * @var bool
     */
    private $status;

    /**
     * Способ получения аттестата
     *
     * 0 - Аттестат выдан после личной встречи с аттестатором (устанавливается только после вторичной проверки)
     * 1 - Аттестат выдан по нотариально заверенным документам (устанавливается только после вторичной проверки)
     * 2 - Аттестат выдан автоматически, по результатам успешного пополнения кошелька (только начальные аттестаты)
     *
     * @var string
     */
    private $notary;

    /**
     * @param int        $id
     * @param int        $type
     * @param Registrant $registrant
     * @param bool       $rightToIssue
     * @param bool       $admRightToIssue
     * @param bool       $recalled
     * @param string     $dateIssuance
     * @param int        $dateDiff
     * @param bool       $status
     * @param string     $notary
     */
    public function __construct(
        int $id,
        int $type,
        Registrant $registrant,
        bool $rightToIssue,
        bool $admRightToIssue,
        bool $recalled,
        string $dateIssuance,
        int $dateDiff,
        bool $status,
        string $notary
    ) {
        $this->id = $id;
        $this->type = $type;
        $this->registrant = $registrant;
        $this->rightToIssue = $rightToIssue;
        $this->admRightToIssue = $admRightToIssue;
        $this->recalled = $recalled;
        $this->dateIssuance = $dateIssuance;
        $this->dateDiff = $dateDiff;
        $this->status = $status;
        $this->notary = $notary;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @return Registrant
     */
    public function getRegistrant(): Registrant
    {
        return $this->registrant;
    }

    /**
     * @return bool
     */
    public function getRightToIssue(): bool
    {
        return $this->rightToIssue;
    }

    /**
     * @return bool
     */
    public function getAdmRightToIssue(): bool
    {
        return $this->admRightToIssue;
    }

    /**
     * @return bool
     */
    public function getRecalled(): bool
    {
        return $this->recalled;
    }

    /**
     * @return string
     */
    public function getDateIssuance(): string
    {
        return $this->dateIssuance;
    }

    /**
     * @return int
     */
    public function getDateDiff(): int
    {
        return $this->dateDiff;
    }

    /**
     * @return bool
     */
    public function getStatus(): bool
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getNotary(): string
    {
        return $this->notary;
    }
}
