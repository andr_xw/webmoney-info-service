<?php

namespace App\DTO;

use DateTimeImmutable;

class UserInfo
{
    /**
     * @var Name
     */
    private $name;

    /**
     * @var Passport
     */
    private $passport;

    /**
     * @var DateTimeImmutable
     */
    private $birthDate;

    /**
     * @var string phone
     */
    private $contactPhone;

    /**
     * @param Name              $name
     * @param Passport          $passport
     * @param DateTimeImmutable $birthDate
     * @param string            $contactPhone
     */
    public function __construct(
        Name $name,
        Passport $passport,
        DateTimeImmutable $birthDate,
        string $contactPhone
    ) {
        $this->name = $name;
        $this->passport = $passport;
        $this->birthDate = $birthDate;
        $this->contactPhone = $contactPhone;
    }

    /**
     * @return Name
     */
    public function getName(): Name
    {
        return $this->name;
    }

    /**
     * @return Passport
     */
    public function getPassport(): Passport
    {
        return $this->passport;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getBirthDate(): DateTimeImmutable
    {
        return $this->birthDate;
    }

    /**
     * @return string
     */
    public function getContactPhone(): string
    {
        return $this->contactPhone;
    }
}
