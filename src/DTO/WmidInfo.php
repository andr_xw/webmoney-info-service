<?php

namespace App\DTO;

class WmidInfo
{
    /**
     * @var string
     */
    private $wmid;

    /**
     * @var bool
     */
    private $isFullAccess;

    /**
     * @var Certificate
     */
    private $passport;

    /**
     * @var UserInfo
     */
    private $userInfo;

    /**
     * @param string      $wmid
     * @param bool        $isFullAccess
     * @param Certificate $passport
     * @param UserInfo    $userInfo
     */
    public function __construct(string $wmid, bool $isFullAccess, Certificate $passport,  $userInfo)
    {
        $this->wmid = $wmid;
        $this->isFullAccess = $isFullAccess;
        $this->passport = $passport;
        $this->userInfo = $userInfo;
    }

    /**
     * @return string
     */
    public function getWmid(): string
    {
        return $this->wmid;
    }

    /**
     * @return bool
     */
    public function isFullAccess(): bool
    {
        return $this->isFullAccess;
    }

    /**
     * @return Certificate
     */
    public function getPassport(): Certificate
    {
        return $this->passport;
    }

    /**
     * @return UserInfo
     */
    public function getUserInfo(): UserInfo
    {
        return $this->userInfo;
    }
}
