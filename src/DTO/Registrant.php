<?php

namespace App\DTO;

class Registrant
{
    /**
     * Внутренний id регистратора
     *
     * @var int
     */
    private $id;

    /**
     * WMID регистратора
     *
     * @var string
     */
    private $wmid;

    /**
     * Название или имя регистратора
     *
     * @var string
     */
    private $nickname;

    /**
     * @param int    $id
     * @param string $wmid
     * @param string $nickname
     */
    public function __construct(int $id, string $wmid, string $nickname)
    {
        $this->id = $id;
        $this->wmid = $wmid;
        $this->nickname = $nickname;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getWmid(): string
    {
        return $this->wmid;
    }

    /**
     * @return string
     */
    public function getNickname(): string
    {
        return $this->nickname;
    }
}
