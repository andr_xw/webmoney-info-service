<?php

namespace App\DTO;

class Passport
{
    /**
     * Название государства, выдавшее паспорт
     *
     * @var string
     */
    private $countryName;

    /**
     * Код или наименование подразделения (органа), выдавшего паспорт
     *
     * @var string
     */
    private $placeIssue;

    /**
     * Дата выдачи паспорта в формате мм/дд/гггг
     *
     * @var string
     */
    private $dateIssue;

    /**
     * Серия и номер паспорта
     *
     * @var string
     */
    private $number;

    /**
     * @param string $countryName
     * @param string $placeIssue
     * @param string $dateIssue
     * @param string $number
     */
    public function __construct(string $countryName, string $placeIssue, string $dateIssue, string $number)
    {
        $this->countryName = $countryName;
        $this->placeIssue = $placeIssue;
        $this->dateIssue = $dateIssue;
        $this->number = $number;
    }

    /**
     * @return string
     */
    public function getCountryName(): string
    {
        return $this->countryName;
    }

    /**
     * @return string
     */
    public function getPlaceIssue(): string
    {
        return $this->placeIssue;
    }

    /**
     * @return string
     */
    public function getDateIssue(): string
    {
        return $this->dateIssue;
    }

    /**
     * @return string
     */
    public function getNumber(): string
    {
        return $this->number;
    }
}
