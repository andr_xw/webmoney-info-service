<?php

namespace App\Controller;

use App\Service\WebMoneyPassportInterface;
use App\DTO\WmidInfo;
use FOS\RestBundle\Controller\Annotations as Rest;

class PassportWebMoneyController
{
    /**
     * @var WebMoneyPassportInterface
     */
    protected $webMoneyPassport;

    /**
     * @param WebMoneyPassportInterface $webMoneyPassport
     */
    public function __construct(WebMoneyPassportInterface $webMoneyPassport)
    {
        $this->webMoneyPassport = $webMoneyPassport;
    }

    /**
     * @Rest\Get("/wmid/{wmid}")
     *
     * @param string $wmid
     *
     * @return WmidInfo
     */
    public function getInfoByWmid(string $wmid): WmidInfo {
        return $this->webMoneyPassport->getInfoByWmid($wmid);
    }

    /**
     * @Rest\Get("/purse/{purse}")
     *
     * @param string $purse
     *
     * @return WmidInfo
     */
    public function getInfoByPurse(string $purse): WmidInfo {
        return $this->webMoneyPassport->getInfoByPurse($purse);
    }
}
