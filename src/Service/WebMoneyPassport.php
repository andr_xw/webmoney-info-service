<?php

namespace App\Service;

use App\DTO\Name;
use App\DTO\Passport;
use App\DTO\WmidInfo;
use App\DTO\Certificate;
use App\DTO\Registrant;
use App\DTO\UserInfo;
use App\Exception\AccessNotFullWebMoneyException;
use App\Exception\RequestWebMoneyException;
use baibaratsky\WebMoney\Api\X\Request as XRequest;
use baibaratsky\WebMoney\Api\X\X8\Request as X8Request;
use baibaratsky\WebMoney\Api\X\X8\Response as X8Response;
use baibaratsky\WebMoney\Api\X\X11\Request as X11Request;
use baibaratsky\WebMoney\Api\X\X11\Response as X11Response;
use baibaratsky\WebMoney\Exception\ApiException;
use baibaratsky\WebMoney\Exception\CoreException;
use baibaratsky\WebMoney\Request\AbstractResponse;
use baibaratsky\WebMoney\Signer;
use baibaratsky\WebMoney\WebMoney;
use DateTimeImmutable;
use Exception;

class WebMoneyPassport implements WebMoneyPassportInterface
{
    /**
     * The flag means that the owner of the verified WMID added the verifier to
     * the list of trusted persons, which allows viewing his personal information
     */
    private const FLAG_FULL_ACCESS = 1;

    /**
     * Successful query execution code
     */
    private const CODE_SUCCESS_REQUEST = 0;

    /**
     * @var string
     */
    private $signerWmid;

    /**
     * @var Signer
     */
    private $signer;

    /**
     * @var WebMoney
     */
    private $wmClient;

    /**
     * @param string   $signerWmid
     * @param string   $signerKeyFilePath
     * @param string   $signerKeyPassword
     *
     * @param WebMoney $wmClient
     *
     * @throws Exception
     */
    public function __construct(
        WebMoney $wmClient,
        string $signerWmid,
        string $signerKeyFilePath,
        string $signerKeyPassword
    ) {
        $this->signerWmid = $signerWmid;
        $this->signer = new Signer($signerWmid, $signerKeyFilePath, $signerKeyPassword);
        $this->wmClient = $wmClient;
    }

    /**
     * @param string $wmid
     *
     * @return WmidInfo
     *
     * @throws AccessNotFullWebMoneyException
     * @throws ApiException
     * @throws CoreException
     * @throws RequestWebMoneyException
     */
    public function getInfoByWmid(string $wmid): WmidInfo
    {
        $request = new X11Request();
        $request->setPassportWmid($wmid);

        /** @var X11Response $response */
        $response = $this->sendRequest($request);

        if (self::FLAG_FULL_ACCESS !== $response->getHasFullAccess()) {
            throw new AccessNotFullWebMoneyException(
                "The owner of the verified WMID $wmid did not add the WMID $this->signer of the trusted list"
            );
        }

        $certificate = $response->getPassport();
        $userInfo = $response->getUserInfo();

        $birthDate = "{$userInfo->getBirthDay()}-{$userInfo->getBirthMonth()}-{$userInfo->getBirthYear()}";

        return new WmidInfo(
            $response->getWmid(),
            (bool) $response->getHasFullAccess(),
            new Certificate(
                $certificate->getId(),
                $certificate->getType(),
                new Registrant(
                    $certificate->getRegistrantId(),
                    $certificate->getRegistrantWmid(),
                    $certificate->getRegistrantNickname()
                ),
                $certificate->getRightToIssue(),
                $certificate->getAdmRightToIssue(),
                $certificate->getRecalled(),
                $certificate->getIssuanceDt(),
                $certificate->getIssuanceDiff(),
                $certificate->getStatus() == 11,
                $certificate->getNotary()
            ),
            new UserInfo(
                new Name(
                    $userInfo->getLastName(),
                    $userInfo->getFirstName(),
                    $userInfo->getMiddleName()
                ),
                new Passport(
                    $userInfo->getCountry(),
                    $userInfo->getPassportPlace(),
                    $userInfo->getPassportIssueDate(),
                    $userInfo->getPassportNum()
                ),
                DateTimeImmutable::createFromFormat('d-m-Y', $birthDate),
                $userInfo->getContactPhone()
            )
        );
    }

    /**
     * @param string $purse
     *
     * @return WmidInfo
     *
     * @throws AccessNotFullWebMoneyException
     * @throws ApiException
     * @throws CoreException
     * @throws RequestWebMoneyException
     */
    public function getInfoByPurse(string $purse): WmidInfo
    {
        $request = new X8Request();
        $request->setPurse($purse);

        /** @var X8Response $response */
        $response = $this->sendRequest($request);

        return $this->getInfoByWmid($response->getWmid());
    }

    /**
     * @param XRequest $request
     *
     * @return AbstractResponse
     *
     * @throws CoreException
     * @throws RequestWebMoneyException
     */
    private function sendRequest(XRequest $request): AbstractResponse
    {
        $request->setSignerWmid($this->signerWmid);
        $request->sign($this->signer);

        $response = $this->wmClient->request($request);

        $returnCode = $response->getReturnCode();

        if (self::CODE_SUCCESS_REQUEST !== $returnCode) {
            throw new RequestWebMoneyException($response->getReturnDescription(), $returnCode);
        }

        return $response;
    }
}
