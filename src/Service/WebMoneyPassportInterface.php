<?php

namespace App\Service;

use App\DTO\WmidInfo;

interface WebMoneyPassportInterface
{
    /**
     * @param string $wmid
     *
     * @return WmidInfo
     */
    public function getInfoByWmid(string $wmid): WmidInfo;

    /**
     * @param string $purse
     *
     * @return WmidInfo
     */
    public function getInfoByPurse(string $purse): WmidInfo;
}
